(1) I think the most difficult part was understanding how this
module should work and what it should do exactly. After we started
understanding and started writing a little code, the idea got
clearer and we knew what we are going to do.

In my opinion the thing that helped most was the examples of using
express from class. From these examples we learned how the user should
use our module, and it helped us understanding what functionality we
are providing to the user.


(2) The most fun part was to see the server actually work. When we
sent HTTP request to it and got good responses it was very satisfying.
Also It was fun to understand deeply the HTTP format and to know how to
handle it.


(3) To test our result we used 2 ways using test.js that implements a
server using our module:

a) First, we used the browser and typed url to the server. The browser
created HTTP requests and we printed it in test.js and printed our results
and other imported things and checked if it was fine.

b) I wrote code that generates HTTP request as I wish and send them to the
server. In this way we were able to check some other functionality, like
body in the request.
Here is the code:

/**
 **************************************** client.js ********************************************
 *
 *
 *
 * Created by tomn96 on 1/9/17.
 */


var net = require('net');

var client = new net.Socket();
client.connect(8080, '127.0.0.1', function() {

    console.log('\r\nConnected\r\n');

    var msg = 'GET /check HTTP/1.1\r\nHost: localhost:8080\r\nContent-Length: 12\r\nContent-Type: text/plain\r\n\r\nHello Server';

    client.write(msg);
    console.log('Sent:\r\n' + msg + '\r\n');
});

client.on('data', function(data) {
    console.log('Received: \r\n' + data + '\r\n');
    client.destroy(); // kill client after server's response
});

client.on('close', function() {
    console.log('\r\nConnection closed\r\n');
});


/**
 ************************************************************************************************
 */
