/**
 ****************************************************************************
 * FILE: hujiwebserver.js
 * WRITERS: Tom Nissim , tomn96 , 208307470
 *          Roni Zhelenchuk, roni93, 308533967
 * EXERCISE: 67555 INTERNET TECHNOLOGIES ex3 2016-2017
 * Description: This file is a node file that defines the code of the
 *              hujiwebserver module that we need to write in ex3.
 ****************************************************************************
 */





var net = require('net');





///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// EXPORT ////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////


module.exports = new hujiwebserver();





///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// HUJI WEB SERVER ///////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////


function hujiwebserver()
{
    this.commands = [];
}


/**
 * this function gets from the user the command and the middleware, and add them into the "commands" data structure
 */
hujiwebserver.prototype.use = function(command, middleware)
{

    if (typeof middleware === 'undefined')
    {
        middleware = command;
        command = '/';
    }

    this.commands.push(
        {
            comm : command,
            midd : middleware
        }
    );
    return this;
};


hujiwebserver.prototype.start = function(port, callback)
{
    var so = new ServerObj(port, callback, this);
    so.listen();
    return so;
};





///////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// SERVER OBJECT ////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////


function ServerObj(port, callback, hws)
{
    this.port = port;
    this.callback = callback;

    this.server = net.createServer(function(socket)
    {
        var msg_command_and_headers = '';
        var msg_body = '';
        var onBody = false;
        var contentLen = null;

        socket.on('data', function(data)
        {
            if (!onBody) //this part is for if the data is coming in parts (more then one time for single request). it
                //also can be only one time
            {
                var lines = data.split('\r\n');
                var j = 0;
                while (j < lines.length && !onBody) {
                    var cl = lines[j].search(new RegExp('Content-Length', 'i')); //getting the length of the request
                    if (cl === 0)
                    {
                        contentLen = parseInt(lines[j].split(': ')[1]);
                    }
                    else if (lines[j] === '') { //j will be the line that the body starts
                        onBody = true;
                    }
                    msg_command_and_headers += lines[j] + '\r\n';
                    j++;
                }

                msg_body += lines.slice(j).join('\r\n');
            }
            else
            {
                msg_body += data;
            }

            if ((contentLen === null && onBody) || msg_body.length >= contentLen) //we done with reading the data from
                //the socket
            {
                try
                {
                    socketHendler(hws, socket, msg_command_and_headers + msg_body);
                } catch (err)
                {
                    var res = new Response(socket); //we need to make new response too return error and the .json() will
                    //close the connection
                    res.status(500).json(err);
                }
            }
        });

        socket.on('error', function() {
            try
            {
                var res = new Response(socket);
                res.status(500).send("<h1>Socket Error</h1>", 'text/html');
            } catch(e) {}
        });

        socket.setTimeout(25000, function()
        {
            var res = new Response(socket);
            res.status(500).send("<h1>Socket Timeout</h1>", 'text/html'); // closes the connection
        });

        socket.setEncoding('utf8');

        socket.on('end', function() {
            socket.destroy();
        });

    });

    this.server.on('error', function(err) {
        // console.log('server error');
        // console.log(err);
    });
}


/**
 * gets the full data from the socket, and handle with the data, create proper request/response and looking for the besr
 * Command from the data structure - "commands"
 */
function socketHendler(hws, socket, data)
{
    var req = new Request(data);
    var res = new Response(socket);
    var matched = false;
    var firstMatch = true; //we start checking if there wasn't any call for .send() or .json() from the first middleware
    for (var i = 0; i < hws.commands.length; i++)
    {
        var params = {};
        if (matchCommand(hws.commands[i].comm, req.path, params)) //looking for the right command
        {
            matched = true;
            req.params = params;
            var next = false;
            if (firstMatch)
            {
                setTimeout(function () { //no one call .send() or .json() after a 10 seconds
                    if (!res.sent)
                    {
                        res.status(404).send("<h1>404 Timeout</h1>", 'text/html');
                    }
                }, 10000);
            }

            hws.commands[i].midd(req, res, function() { next = true; }); //if the user asked for next
            if(!next) { break; }
            req.params = {};
            firstMatch = false;
        }
    }

    if(!matched) //didn't found any right command
    {
        res.status(404).send("<h1>404 Not found</h1>", 'text/html');
    }
}


ServerObj.prototype.stop = function()
{
    this.server.close();
};


ServerObj.prototype.listen = function()
{
    this.server.listen(this.port, this.callback);
};


/**
 * looking for the best/right command to the path that we got from the socket. after finding the right one
 * checking if there any params, and saves them if needed
 */
function matchCommand(command, path, params)
{
    var replace = command.replace(new RegExp(':[^/]+', 'g'), '[^/]+');
    var regExp = new RegExp(replace);
    var res = regExp.exec(path);
    if (res != null && res.index == 0)
    {
        var commSplit = command.split('/');
        var pathSplit = path.split('/');

        commSplit = commSplit[commSplit.length - 1] === '' ? commSplit.slice(0,-1) : commSplit;
        pathSplit = pathSplit[pathSplit.length - 1] === '' ? pathSplit.slice(0,-1) : pathSplit;


        if (pathSplit.length < commSplit.length)
        {
            return false;
        }

        for (var i = 0; i < commSplit.length; i++)
        {
            if (commSplit[i].search(':') === 0)  // same as startsWith(':')
            {
                params[commSplit[i].slice(1)] = pathSplit[i]; //save the param
            }
            else if (commSplit[i] != pathSplit[i])
            {
                return false;
            }
        }
        return true;
    }
    return false;
}





///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Request ///////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////


/**
 * gets the path and the query from the http request (from the first line of the request)
 */
function uriParser(URIRequest)
{
    var res = {};
    var path_and_queries = URIRequest.split('?');

    res.path = path_and_queries[0];
    res.query = {};

    //saving the parameters of the query
    if (path_and_queries.length > 1 && path_and_queries[1].length > 0) // if there is a query
    {
        var queries = path_and_queries[1].split('&');

        for (var i = 0; i < queries.length; i++) {
            var key_and_value = queries[i].split('=');
            res.query[key_and_value[0]] = key_and_value[1].replace('+', ' '); //saving the query
        }
    }

    return res;
}


/**
 * fill the method, path,query, protocol and version of the request object, by parsing the first line
 */
function requestFirstLineParser(firstLine, req)
{
    var firstLineSplit = firstLine.split(" ");

    req.method = firstLineSplit[0];

    var URIParserData = uriParser(firstLineSplit[1]);
    req.path = URIParserData.path;
    req.query = URIParserData.query;

    var protocol_and_version = firstLineSplit[2].split('/');
    req.protocol = protocol_and_version[0].toLowerCase();
    req.version = protocol_and_version.length > 0 ? protocol_and_version[1] : null;
}


function requestParser(str, req)
{
    var lines = str.split('\r\n');
    requestFirstLineParser(lines[0], req); //see above

    req.headers = {};
    var lineIdx = 1; // skip the first line
    // saves all of the headers
    for (; lineIdx < lines.length && lines[lineIdx] != ''; lineIdx++)
    {
        var header = lines[lineIdx].split(': ');
        req.headers[header[0].toLowerCase()] = header[1];  // using toLowerCase for case insensitive functionality
    }

    //saves the body part of the request
    req.body = null;
    lineIdx++;  // skip the empty line
    if(lineIdx < lines.length)  // if there is a body
    {
        req.body = lines.slice(lineIdx).join('\r\n');
    }
}


/**
 * Parse the cookie part, and saves the cookies in the right structure
 */
function cookieParser(cookieStr)
{
    var res = {};
    if (cookieStr === undefined) //if there isn't any cookies
    {
        return res;
    }

    var cookies = cookieStr.split('; ');
    for (var i = 0; i < cookies.length; i++)
    {
        var key_and_value = cookies[i].split('=');
        res[key_and_value[0]] = key_and_value[1];
    }
    return res;
}


/**
 * @returns the host name
 */
function hostName(hostStr)
{
    if (hostStr === undefined)
    {
        return undefined;
    }
    return hostStr.split(':')[0];
}


/**
 * gets the http request and parse it, and create the Request object.
 */
function Request(str)
{
    requestParser(str, this);
    this.params = undefined; //the params (is exist) are created in matchCommand method after we found the right command
    this.cookies = cookieParser(this.get('Cookie'));
    this.host = hostName(this.get('Host'));
}


Request.prototype.get = function(key)
{
    if(key === undefined)
    {
        return undefined;
    }
    return this.headers[key.toLowerCase()];  // using toLowerCase for case insensitive functionality
};


/**
 * The param method is looking for the key in the params and in the query, the defaultValue is optional
 */
Request.prototype.param = function(key, defaultValue)
{
    var param = this.params[key];
    if (param != undefined)
    {
        return param;
    }

    var query = this.query[key];
    if (query != undefined)
    {
        return query;
    }
    return defaultValue;
};


Request.prototype.is = function(type)
{
    type.replace('*', '.*');
    var regexp = new RegExp(type);
    return regexp.test(this.get('Content-Type'));
};





///////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// Response //////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////


/**
 * creates the response object
 */
function Response(socket)
{
    this.socket = socket;

    this.statusCode = 200; //default
    this.statusMsg = 'OK';
    this.headers = {};
    this.cookies = {};
    this.body = null;
    this.sent = false;
}


Response.prototype.set = function(key, value)
{
    this.headers[key] = value;
    return this;
};


Response.prototype.get = function(key)
{
    for (var k in this.headers)
    {
        if (this.headers.hasOwnProperty(k) && k.toLowerCase() === key.toLowerCase())  // using toLowerCase for case insensitive functionality
        {
            return this.headers[k]
        }
    }
    return undefined;
};


Response.prototype.status = function (statusCode)
{
    switch (statusCode)
    {
        case 200:
            this.statusMsg = 'OK';
            break;
        case 400:
            this.statusMsg = 'Bad Request';
            break;
        case 404:
            this.statusMsg = 'Not found';
            break;
        case 500:
            this.statusMsg = 'Internal Server Error';
            break;
        default:
            this.statusMsg = '';
    }
    this.statusCode = statusCode;
    return this;
};


Response.prototype.cookie = function(name, value)
{
    this.cookies[name] = value;
    return this;
};


/**
 * sending the response, and adding the body
 */
Response.prototype.send = function(body_str, type)
{

    if (typeof body_str === 'object')
    {
        this.json(body_str);
        return;
    }

    body_str = (typeof body_str === 'undefined') ? this.body : body_str;

    if (typeof type != 'undefined')
    {
        this.set('Content-Type', type);
    }

    if (this.get('Content-Length') === undefined && body_str != null)
    {
        this.set('Content-Length', body_str.length);
    }


    var result = "HTTP/1.1 " + this.statusCode + ' ' + this.statusMsg + '\r\n';

    for (var header in this.headers)
    {
        if (this.headers.hasOwnProperty(header))
        {
            result += header + ': ' + this.headers[header] + '\r\n';
        }
    }

    if (this.cookies.length > 0)
    {
        result += 'Cookie: ';
        var i = 0;
        for (var cookie in this.cookies)
        {
            if (this.cookies.hasOwnProperty(cookie))
            {
                if (i === this.cookies.length - 1)
                {
                    result += cookie + '=' + this.cookies[cookie];
                } else
                {
                    result += cookie + '=' + this.cookies[cookie] + '; ';
                }
                i++;
            }
        }
        result += '\r\n';
    }
    result += '\r\n';
    if (body_str != null)
    {
        result += body_str;
    }

    //we have to close the socket after we send the response and send the result(http 1.1)
    try
    {
        this.socket.end(result);
    }
    catch (e) {}

    this.sent = true;
};


Response.prototype.json = function(jsonObj)
{
    this.send(JSON.stringify(jsonObj), 'application/json');
};
