/**
 **************************************** client.js ********************************************
 *
 *
 *
 * Created by tomn96 on 1/9/17.
 */


var net = require('net');

var client = new net.Socket();
client.connect(8080, '127.0.0.1', function() {

    console.log('\r\nConnected\r\n');

    var msg = 'GET /check HTTP/1.1\r\nHost: localhost:8080\r\nContent-Length: 12\r\nContent-Type: text/plain\r\n\r\nHello Server';

    client.write(msg);
    console.log('Sent:\r\n' + msg + '\r\n');
});

client.on('data', function(data) {
    console.log('Received: \r\n' + data + '\r\n');
    client.destroy(); // kill client after server's response
});

client.on('close', function() {
    console.log('\r\nConnection closed\r\n');
});


/**
 ************************************************************************************************
 */
