/**
 ****************************************************************************
 * FILE: test.js
 * WRITERS: Tom Nissim , tomn96 , 208307470
 *          Roni Zhelenchuk, roni93, 308533967
 * EXERCISE: 67555 INTERNET TECHNOLOGIES ex3 2016-2017
 * Description: This file is a node file that demonstrates a simple usage
 *              of the hujiwebserver module that we wrote.
 ****************************************************************************
 */





var fs = require('fs');


var hws = require('./hujiwebserver');

hws.use('/hello/world', function (req,res) {

    res.set('Content-Type', 'text/plain');
    res.send('hello world');
});

hws.use('/add/:n/:m', function (req,res) {
    res.json( { result: parseInt(req.param('n')) * parseInt(req.param('m')) } );
});


hws.use('/filez/', function (req, res) {
    var type1 = req.path.split('.');
    var type2 = type1[type1.length - 1];

    var type = undefined;

    switch (type2)
    {
        case 'js':
            type = 'application/javascript';
            break;
        case 'html':
            type = 'text/html';
            break;
        case 'css':
            type = 'text/css';
            break;
    }

    fs.readFile('.' + req.path, 'utf8', function (err, data) {
        if(err)
        {
            res.status(404).send("<h1>404 File not found</h1>", 'text/html');
            return;
        }
        res.set('Content-Type', type);
        res.send(data);
    });
});


hws.use('/first/', function (req, res, next)
{
    // console.log('first middleware');
    next();
});

hws.use('/first/second', function (req, res)
{
    // console.log('second middleware');
    res.send('second middleware', 'text/plain');
});


hws.use('/tryNoSend', function ()
{
    // console.log('No send - check');
});


hws.use('/trySendObject', function (req, res)
{
    // console.log('using the "send" method to send an object');
    var names = {name1: 'Tom Nissim', name2: 'Roni Zhelenchuk'};
    res.send(names);
});


var port = 8080;
hws.start(port, function () {
    // console.log('started listening on port: ' + port);
});
