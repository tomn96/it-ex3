(1) What was hard in this ex?
The hard part in the ex, was to understanding how the server should work,
 How we get the data and create proper response to every request and how the program should flow in general.

(2) What was fun in this ex? (We won’t reduce points in case this part is empty)
Yes. It was fun to learn new things.

(3) how did you test your server and include as details result as much as possible
We created our own testers - we create new middlewares with different parameters, and checked if there are proper commands
for those middleware, what they need to return/print, we also checked the methods of REQUEST and RESPONSE.
we also created few middlewares, with the same prefix and check if "turn on" the next method
we found the relevant middleware. out testing also included files, folders that doesn't exist. We check if out server
still holding on for every different request if it is legal or not.